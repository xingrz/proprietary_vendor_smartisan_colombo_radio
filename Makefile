DEVICE := colombo

MODEM_IMAGE := firmware-update/NON-HLOS.bin
XBL_ELF := firmware-update/xbl.elf
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_ELF) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(XBL_ELF) $(APPSBOOT_MBN)
ifneq ($(HASH_XBL), 8868589fcbb8e9dae4075f65683992e1f7b76562)
	$(error SHA-1 of xbl.elf mismatch)
endif
ifneq ($(HASH_APPSBOOT), 14a2e809eed4f4ddde60abb46712da87210cba28)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
